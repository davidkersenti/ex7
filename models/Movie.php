<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property integer $id
 * @property string $name
 * @property string $genre
 * @property integer $min_age
 * @property integer $grade
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min_age', 'grade'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['genre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'genre' => 'Genre',
            'min_age' => 'Min Age',
            'grade' => 'Grade',
        ];
    }
}
